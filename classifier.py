import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize
from sklearn.metrics import accuracy_score, confusion_matrix, precision_score, recall_score, f1_score
from sklearn.preprocessing import StandardScaler
# Fix: added missing imports
from collections import Counter
import random
from sklearn.utils import check_random_state

# Fix: adjusted entire file to PEP8 for improved readability

##########################
#  Feature computation
##########################

'''
We additionally take a testing_documents parameter in order to continue working in an object-oriented style. 
Considering, that our test data is given at training time, no issues arise. 
In practical settings we would need to refactor our code. 
'''

class FeatureComputer():
    def __init__(self, documents, testing_documents=None):
        self.docs = self.load_documents(documents)
        self.vocab, self.global_vocab = self.extract_vocabulary()
        self.idf = self.compute_idf()
        self.tf_idf = self.compute_tf_idf(self.docs)
        self.vocab_index = self.get_vocab_index()
        if testing_documents:
            self.test_docs = self.load_documents(testing_documents)
            self.test_tf_idf = self.compute_tf_idf(self.test_docs)

    def simple_features(self, document):
        """ Compute the simple features, i.e., number of sentences,
        the average number of words per sentence,
        and the average number of characters per word. """
        sentences = sent_tokenize(document)
        num_sent = len(sentences)
        mean_words = np.mean([len(word_tokenize(sent)) for sent in sentences])
        mean_chars = np.mean([len(word) for word in word_tokenize(document)])
        return num_sent, mean_words, mean_chars

    # Fixes: added missing import
    # Results dict first goes over documents then over words (words contains all words with their respective counts)
    def load_documents(self, documents):
        """ Index and load documents """
        results = {}
        index = 0
        for doc, label in documents:
            results[index] = {'words': Counter(word_tokenize(doc)), 'label': label, 'doc': doc}
            index += 1
        return results

    """ Compute a dictionary indexing the vocabulary """

    '''Fixes: change vocabulary to set, added vocab_dict for each document
    '''

    def extract_vocabulary(self):
        global_vocab = set()
        vocab_dict = {}
        for key, val in self.docs.items():
            vocab = set()
            for word in val['words'].keys():
                vocab.add(word)
                global_vocab.add(word)
            vocab_dict[key] = vocab
        # global vocab to list to ensure fixed ordering
        return vocab_dict, list(global_vocab)

    # Creates a word index mapping for each word inside our vocabulary
    def get_vocab_index(self):
        word_index = {}
        for index, word in enumerate(self.global_vocab):
            word_index[word] = index
        return word_index

    '''Fixes: Implemented function in a different manner, 
    documents are attributes of our class, thus passing them as parameters is not necessary
    '''

    def compute_idf(self):
        """ Compute inverse document frequency dict for all words across
        all documents"""

        # Define number of documents, we calculate test scores only based on test data (black box setting)
        number_of_documents = len(self.docs)
        inverse_document_frequency = {}
        document_frequency = {}
        # Compute document frequency of each term
        for word in self.global_vocab:
            # Avoid loops if possible to speed up computation
            document_frequency[word] = np.sum([1 for doc in self.vocab.values() if word in doc])
            inverse_document_frequency[word] = np.log(number_of_documents / document_frequency[word])

        return inverse_document_frequency

    def compute_tf_idf(self, docs):
        """ Compute tf-idf values for all words across all documents"""
        results = {}
        for doc in docs.keys():
            total_number_of_terms_in_doc = sum(docs[doc]['words'].values())
            # Define dict to buffer save TF-IDF scores
            scores_dict = {}
            for word in self.global_vocab:
                # Calculate TF-IDF according to the definition
                scores_dict[word] = docs[doc]['words'][word] / total_number_of_terms_in_doc * self.idf[word]
                results[doc] = scores_dict
        return results

    '''Fixes: The sorted function is not needed, since the document dictionary is sorted already, the feature length 
    should not correspond to the vocab_index, initializing the feature array with zeros is also suboptimal, considering
    we would be leaving an unused zeros (unless we handle this explicitly), the code indexed the feature vector with
    the vocabulary index of our current word and set it to 0. The reason for this was not apparent. 
    '''

    def get_features_train(self):
        """ Compute training features for training data """
        examples = {}
        for doc, document in self.docs.items():
            # Add simple features
            feature = np.array(self.simple_features(document['doc']))
            # Add TF-IDF features
            tf_idf_features = [tf_idf_value for tf_idf_value in self.tf_idf[doc].values()]
            feature = np.append(tf_idf_features, feature)
            examples[doc] = {'feature': feature, 'label': document['label']}
        return examples

    ''' Fixes: First parameter of a method is named self, testdata being a parameter is redundant now, We also decided
    against using the imputer, since just setting the value to 0 is more reasonable. Our reasoning is as follows:
    The case of a word not existing in the test set and training set is very similar to a word in the training set not
    occurring in a document. Thus the most reasonable setting is to set the TF-IDF value to 0
    '''
    '''We note, that this function is very similar to the function above. This is a redundancy, which should be refactored
    in the future.
    '''

    def get_features_test(self):  # TODO
        examples = {}
        for doc, document in self.test_docs.items():
            # Add simple features
            feature = np.array(self.simple_features(document['doc']))
            # Add TF-IDF features
            tf_idf_features = [tf_idf_value for tf_idf_value in self.test_tf_idf[doc].values()]
            # compute feature vector and corresponding dictionary
            feature = np.append(tf_idf_features, feature)
            examples[doc] = {'feature': feature, 'label': document['label']}
        return examples

    def get_number_of_words(self, docs, testing=False):
        word_count = [sum(doc['words'].values()) for doc in self.docs.values()]

        # Different value for testing dataset
        if testing:
            word_count = [sum(doc['words'].values()) for doc in self.test_docs.values()]

        return word_count


# Achieves 100% on the test data set
def simple_classifier(docs):
    char_lens = [len(str(x)) for x in docs]
    predictions = []
    for char_len in char_lens:
        label = 's\n'
        if char_len > 600:
            label = 'l\n'
        predictions.append(label)

    return predictions


##########################
# Simple helper functions
##########################

# set seeds for reproducibility
def set_seeds(random_seed=48):
    # Set seed for NumPy
    np.random.seed(random_seed)

    # Set seed for Python's built-in random module
    random.seed(random_seed)

    # Set seed for sklearn models
    random_state = check_random_state(random_seed)

    return random_state


# Fixes: Added missing ':'
def get_number_of_characters(text):
    iterator = 0
    character_counter = 0
    while iterator < len(text):
        for word in text[iterator]:
            for character in word:
                character_counter = character_counter + 1
            iterator = iterator + 1
    return character_counter


'''Fixes: changed l to line, changed 'rw' to 'r' ('rw' does not exist, 'r' is read-only), changed return value from data
to result, if firstline statement was always true and bad style due to creating unnecessary dependencies
'''


# Input: data filepath
# Output: data in list: [(text, (unprocessed)label), ..., (text, (unprocessed)label)]
def read_data(data):
    result = []
    log = open(data, 'r')
    lines = log.readlines()
    for line in lines[1:]:
        result.append((line.split('\t')[0], line.split('\t')[1]))
    return result


# Here we compute the best features according to our logistic model
def get_best_features(log_model, k=10):  # TODO
    """ Computes the best feature """
    # Get feature importances from our logistic model (coefficients)
    feature_importance = np.abs(log_model.coef_[0])

    # Sort the features by descending importance
    feature_importance_idx = np.argsort(feature_importance)[::-1]

    return feature_importance, feature_importance_idx[:k]


# function, which encapsulates evaluation metrics
def evaluate_model(predictions, labels):
    # Calculate metrics
    mlp_accuracy = accuracy_score(labels, predictions)
    mlp_conf_matrix = confusion_matrix(labels, predictions)
    mlp_precision = precision_score(labels, predictions, pos_label='l\n')
    mlp_recall = recall_score(labels, predictions, pos_label='l\n')
    mlp_f1 = f1_score(labels, predictions, pos_label='l\n')

    # Print Metrics
    print("Accuracy:", mlp_accuracy)
    print("Confusion Matrix:\n", mlp_conf_matrix)
    print("Precision:", mlp_precision)
    print("Recall:", mlp_recall)
    print("F1 Score:", mlp_f1)


##########################
#       Classifier
##########################

if __name__ == '__main__':
    # set seeds
    random_state = set_seeds(42)
    # Define datapaths
    train_file_path = 'data/train.tsv'
    test_file_path = 'data/test.tsv'

    # Fix: print statement did not have brackets
    print("Loading data...")
    train = read_data(train_file_path)
    test = read_data(test_file_path)

    print("Computing features...")

    feature_comp = FeatureComputer(documents=train, testing_documents=test)
    data_train = feature_comp.get_features_train()
    data_test = feature_comp.get_features_test()

    train_X = np.array([doc['feature'] for key, doc in sorted(data_train.items())])
    train_y = np.array([doc['label'] for key, doc in sorted(data_train.items())])

    test_X = ([doc['feature'] for key, doc in sorted(data_test.items())])
    test_y = [doc['label'] for key, doc in sorted(data_test.items())]

    # Normalize the data using StandardScaler
    scaler = StandardScaler()
    train_X_normalized = scaler.fit_transform(train_X)
    test_X_normalized = scaler.transform(test_X)

    print("Training models...")
    # Logistic Regression model
    logistic_model = LogisticRegression(random_state=random_state, verbose=1)
    logistic_model.fit(train_X, train_y)
    #  with open('models/logistic_model.pkl', 'rb') as file:
    #    logistic_model = pickle.load(file)

    # MLP (Multi-Layer Perceptron) model
    mlp_model = MLPClassifier(random_state=random_state, verbose=True)
    mlp_model.fit(train_X, train_y)
    # with open('models/mlp_model.pkl', 'rb') as file:
    #    mlp_model = pickle.load(file)

    # Making predictions
    logistic_predictions = logistic_model.predict(test_X)
    mlp_predictions = mlp_model.predict(test_X)

    # Evaluate models
    print("Logistic Regression Metrics:")
    evaluate_model(logistic_predictions, test_y)
    print("\nMLP Metrics:")
    evaluate_model(mlp_predictions, test_y)

    print("Calculating best features...")
    feature_importance, feature_importance_idx = get_best_features(logistic_model)

    print(f"Top 10 most important features{feature_importance_idx}")
    voc = np.array(feature_comp.global_vocab)

    # Train models on new/best features
    train_X_best = train_X_normalized[:, feature_importance_idx]
    test_X_best = test_X_normalized[:, feature_importance_idx]

    # Fixes: model was not defined, added missing code
    best_model_logistic = LogisticRegression()
    best_model_logistic.fit(train_X_best, train_y)

    best_model_mlp = MLPClassifier()
    best_model_mlp.fit(train_X_best, train_y)

    # Making predictions
    best_logistic_predictions = best_model_logistic.predict(test_X_best)
    best_mlp_predictions = best_model_mlp.predict(test_X_best)

    # Evaluate Best models
    print("Best Logistic Regression Metrics:")
    evaluate_model(best_logistic_predictions, test_y)
    print("\n Best MLP Metrics:")
    evaluate_model(best_mlp_predictions, test_y)

    # Calculate predictions of the simple classifier
    preds = np.array(simple_classifier(test))
    # Observe that computing the mean here is the same as computing the accuracy
    accuracy_simple = np.mean(test_y == preds)

    print(f'Accuracy of the simple classifier {accuracy_simple}')